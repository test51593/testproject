import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter)
const router  = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/first',
            name: 'first-page',
            component: () =>
            import('@/pages/FirstPage.vue')
        },
        {
            path: '/table',
            name: 'table-page',
            component:() =>
            import('@/pages/TablePage.vue')
        }
    ]
})
export default router