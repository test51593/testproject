import Axios from 'axios'

export default {
    namespaced: true,
    actions:{
        loadData(ctx) {
            return new Promise((resolve, reject)=> {
                Axios
                    .get("https://jsonplaceholder.typicode.com/posts/", {})
                    .then(function (response) {
                        ctx.commit("setProfile", response.data)
                        resolve(response);
                    })
                    .catch(function (error) {
                        console.log(error);
                        reject(error.response.data.errors);
                    });
            })
        },
    },
    mutations: {
        setProfile(state, profile){
            console.log(profile.length);
            state.profile = profile;
        },
    },
    state: {
        profile: {},
    },
    getters: {
        profile: (state) => state.profile,
        getInfo: (state) => {
            return state.profile;
        },
        getUserId: (state) => {
            console.log("getUserId");
            return state.profile.userId;
        },
        getTitle: (state) => {
            console.log("getTitle");
            return state.profile.title;
        },
    }
}